<?php
/**
 * Class DB
 */
class DB
{
    public $dbName = 'educate';
    public $pass = 'root';
    public $user = 'root';

    /**
     * @return PDO
     */
    public function getConnection()
    {

        $c = new PDO("mysql:host=localhost;dbname={$this->dbName}", $this->user, $this->pass, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8'
        ));
        $c->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $c;

    }
}
