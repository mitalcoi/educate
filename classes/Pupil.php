<?php
/**
 * Class Pupil
 * @property string $fio
 * @property int $age
 * @property int $iq
 * @property string $school
 * @property string $interests
 */
class Pupil extends Studying
{

    protected function tableName()
    {
        return 'pupil';
    }

    /**
     * @return string
     */
    public function getSmart()
    {
        if ($this->iq > 50 AND $this->iq < 100) {
            return 'Не особо.';
        } elseif ($this->iq >= 100 AND $this->iq < 130) {
            return 'Умен.';
        } else {
            return 'Очень умен!';
        }
    }

}
