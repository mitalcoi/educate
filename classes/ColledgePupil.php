<?php
/**
 * Class Pupil
 */
class ColledgePupil extends Studying
{

    protected function tableName()
    {
        return 'colledge_pupil';
    }

    /**
     * @param array $data
     */
    public function save(array $data)
    {
        $connection = $this->getDb();
        $sql = "INSERT INTO {$this->tableName()} (fio, interests, iq, school, age) VALUES(:fio, :interests, :iq, :school, :age)";
        $statement = $connection->prepare($sql);
        $statement->bindValue(":fio", $data['fio']);
        $statement->bindValue(":interests", $data['interests']);
        $statement->bindValue(":iq", $data['iq']);
        $statement->bindValue(":school", $data['school']);
        $statement->bindValue(":age", $data['age']);
        $statement->execute();
    }

    /**
     * @return string
     */
    public function getFio()
    {
        return $this->data['fio'];
    }

    /**
     * @return string
     */
    public function getAge()
    {
        return $this->data['age'] . ' лет';
    }

    /**
     * @return string
     */
    public function getSchool()
    {
        return $this->data['school'];
    }

    /**
     * @return string
     */
    public function getSmart()
    {
        if ($this->data['iq'] > 50 AND $this->data['iq'] < 100) {
            return 'Не особо.';
        } elseif ($this->data['iq'] >= 100 AND $this->data['iq'] < 130) {
            return 'Умен.';
        } else {
            return 'Очень умен!';
        }
    }

    public function getInterests()
    {
        return $this->data['interests'];
    }

}
