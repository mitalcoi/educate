<?php
/**
 * Base class.
 * Class Studing
 */
abstract class Studying
{
    public function __isset($name)
    {
        return in_array($name, $this->getColumns());
    }

    public function __get($name)
    {
        if (in_array($name, $this->getColumns())) {
            return $this->data[$name];
        }
    }

    public function __set($name, $value)
    {
        if (in_array($name, $this->getColumns())) {
            $this->data[$name] = $value;
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->data) {
            return false;
        }
        foreach ($this->data as $key => $data) {
            if (!$data) {
                unset($this->data[$key]);
            }
        }
        $keys = array_keys($this->data);
        $keysAsString = join(', ', $keys);
        $valuesAsString = join(', ', $this->data);
        $connection = $this->getDb();
        $sql = "INSERT INTO {$this->tableName()} ($keysAsString) VALUES({$valuesAsString})";
        $statement = $connection->prepare($sql);
        $statement->execute();
        return true;
    }

    /**
     * @var array
     */
    protected $data=array();


    protected function getDb()
    {
        $db = new DB();
        return $db->getConnection();
    }

    protected function getColumns()
    {
        $q = $this->getDb()->prepare("DESCRIBE {$this->tableName()}");
        $q->execute();
        $table_fields = $q->fetchAll(PDO::FETCH_COLUMN);
        return $table_fields;
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string table name
     */
    protected abstract function tableName();

    /**
     * @return Pupil[]
     */
    public function findAll()
    {
        $objects = array();
        $connection = $this->getDb();
        $sql = "SELECT * FROM {$this->tableName()}";
        $alldata = $connection->query($sql)->fetchAll();

        foreach ($alldata as $data) {
            $object = new $this;
            $object->setData($data);
            $objects[] = $object;
        }
        return $objects;
    }

    /**
     * @param $id
     * @return Pupil
     */
    public function findByPk($id)
    {
        $object = new $this;
        $connection = $this->getDb();
        $sql = "SELECT * FROM {$this->tableName()} WHERE id=$id";
        $object->setData($connection->query($sql)->fetch());
        return $object;
    }

    /**
     * @param array $ids
     * @return Pupil[]
     */
    public function findAllByPks($ids = array())
    {
        $objects = array();
        foreach ($ids as $id) {
            $obj = new $this;
            $objects[] = $obj->findByPk($id);
        }
        return $objects;
    }

    public function deleteByPk($id)
    {
        $connection = $this->getDb();
        $sql = "DELETE FROM {$this->tableName()} WHERE id=:id";
        $statement = $connection->prepare($sql);
        $statement->bindValue(":id", $id);
        $statement->execute();
    }
}



