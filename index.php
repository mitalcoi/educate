<?php
require_once('__autoload.php');

$obj = new Pupil();
$pupils = $obj->findAll();
foreach ($pupils as $i => $pupil):
    ?>

    <?php echo '#' . $i; ?>
    <p>Школьник: <?php echo $pupil->fio; ?></p>
    <p>Интересы: <?php echo $pupil->interests; ?></p>
    <p>Возраст: <?php echo $pupil->age; ?> лет</p>
    <p>Школа: <?php echo $pupil->school; ?></p>
    <p>Он умный? <?php echo $pupil->getSmart(); ?></p>
    <a href="delete.php?id=<?= $pupil->id ?>">Удалить ученика #<?= $pupil->id ?></a>
    <hr/>
<? endforeach; ?>
<a href="create.php">Добавить нового ученика</a>